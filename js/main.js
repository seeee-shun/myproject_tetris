document.getElementById("hello_text").textContent="テトリス入門編";

var count = 0;
var cells;
var blocks = {
    i: {
        class: "i",
        pattern: [
            [0,1,0],
            [0,1,1],
            [0,1,1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1,0,0],
            [1,1,1]
        ]
    },
    t:{
        class: "t",
        pattern: [
            [1,1],
            [0,1]
        ]
    },
    s:{
       class: "s",
       pattern: [
           [1],
           [1]
       ]
    },
    z:{
        class: "z",
        pattern: [
            [1,1,1]
        ]
    },
    j:{
        class: "j",
        pattern: [
            [1,1],
            [1,1]
        ]
    },
    l:{
        class: "l",
        pattern: [
            [1,0],
            [1,1],
            
        ]
    }
};

loadTable();
gameDescription();
var timerId = setInterval(function(){
    count++;
    document.getElementById("hello_text").textContent="テトリス入門編(" + count + ")";
    // ブロックが積みあがってないかのチェック  
    for(var col = 0;col < 10; col++){
        var canComplete = true;
        for(var row = 19; row >= 0; row--){
            if(cells[row][col].className === ""){
                canComplete = false;
            }
        }
    
        if(canComplete){
            alert("out");
            document.getElementById("game_over").innerHTML += "<p>GameOver</p>";
            clearInterval(timerId);
            
            }
        }
    if(i === 3){
        document.getElementById("game_clear").innerHTML += "<p>GameClear</p>";
        clearInterval(timerId);
    }
    if(hasFallingBlock()){
        fallBlocks();
    }else{
        deleteRow();
        generateBlock();
    }
},1000);

function gameDescription(){
    var des = "このテトリスは矢印キーで操作します。\n1つの行を消すごとに10pt獲得し、50ptずつでLevelupできます。\n3Lvに到達でゲームクリアです！";
    alert(des);
}
function loadTable(){
    cells = [];
    var td_array = document.getElementsByTagName("td");
    var index = 0;
    for(var row = 0;row < 20;row++){
        cells[row] = [];//二次元配列
        for(var col = 0;col < 10;col++){
            cells[row][col] = td_array[index]
            index++;
        }
    }

}


function fallBlocks(){
    //一番下についてないか
    for(var col=0;col < 10;col++){
        if(cells[19][col].blockNum === fallingBlockNum){
            isFalling = false;
            return;
        }
    }
   //一マス下に別のブロックがないか
   for(var row = 18; row >= 0;row--){
       for(var col = 0; col < 10; col++){
           if(cells[row][col].blockNum === fallingBlockNum){
               if(cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
               isFalling = false ;
               return;   
               }
           }
       }
   }
  
    //下から二番目の行から繰り返しクラスを下げる
    for(var row = 18;row >= 0;row--){
        for(var col = 0;col < 10; col++){
         if(cells[row][col].blockNum === fallingBlockNum){
            console.log(cells[row][col].blockNum );
            cells[row + 1][col].className = cells[row][col].className;
            cells[row + 1][col].blockNum = cells[row][col].blockNum;
            cells[row][col].className = "";
            cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;
function hasFallingBlock(){
　//落下中のブロックがあるかどうかを確認する
  return isFalling;
}

var totalExp = 0;
var levelupExp = 50;
var i = 0;
function deleteRow(){
　//そろっている行を消す
　for(var row = 19; row >= 0; row--){
    var canDelete = true;
    for(var col = 0;col < 10; col++){
        if(cells[row][col].className === ""){
            canDelete = false;
        }
    }
    if(canDelete){
        //一行消す
        for(var col = 0;col < 10;col++){
            cells[row][col].className = "";
        }
        //上の行のブロックを全て一マス落とす
        for(var downRow = row - 1;downRow > 0;downRow--){
            for(var col = 0;col < 10;col++){
            cells[downRow + 1][col].className = cells[downRow][col].className;
            cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
            cells[downRow][col].className = "";
            cells[downRow][col].blockNum = null;
            }
        }
        // ポイントの表示
         var exp = 10;
         totalExp += exp;
         if(totalExp >= levelupExp){
            i++;
            document.getElementById("level_up").textContent = i + "Lv";
            document.getElementById("levelup_point").textContent = "Gameclearには"　+ (3 - i) + "Levelupが必要だ";
            levelupExp += 50;
        }if(totalExp > 0){
            document.getElementById("up_point").textContent = "Levelupまで"　+ (levelupExp - totalExp) + "pt";
        }
    }
  }
}

var fallingBlockNum = 0;
function generateBlock(){
 //ランダムにブロックを生成する
 //ブロックパターンからランダムに1つパターンを選ぶ
 var keys = Object.keys(blocks);
 var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
 var nextBlock = blocks[nextBlockKey];
 var nextFallingBlockNum = fallingBlockNum + 1;
 //選んだパターンをもとにブロック配置
 var pattern = nextBlock.pattern;
 for(var row = 0;row < pattern.length;row++){
     for(var col = 0;col < pattern[row].length;col++){
        if(pattern[row][col]){  
             cells[row][col + 2].className = nextBlock.class;
             cells[row][col + 2].blockNum = nextFallingBlockNum;
         }
     }
 }
 //落下中のブロックがあるとする
 isFalling = true;
 fallingBlockNum = nextFallingBlockNum;
}

function moveLeft(){
   //一マス左にブロックがあるかどうか
   for(var row = 19; row >= 0;row--){
       for(var col = 0; col < 10; col++){
           if(cells[row][col].blockNum === fallingBlockNum){
               if(cells[row][col -1].className !== "" && cells[row][col -1].blockNum !== fallingBlockNum){
               isFalling = false ;
               return;   
               }
           }
       }
   }

    for(var row = 0;row < 20;row++){
        for(var col = 0; col < 10;col++){
            if(cells[row][col].blockNum === fallingBlockNum){
            cells[row][col - 1].className = cells[row][col].className;
            cells[row][col - 1].blockNum = cells[row][col].blockNum;
            cells[row][col].className = "";
            cells[row][col].blockNum = null;
            }
        }
    }
}

function moveLight(){
   //一マス右にブロックがあるかどうか
   for(var row = 19; row >= 0;row--){
       for(var col = 0; col < 10; col++){
           if(cells[row][col].blockNum === fallingBlockNum){
               if(cells[row][col + 1].className !== "" && cells[row][col + 1].blockNum !== fallingBlockNum){
               isFalling = false ;
               return;   
               }
           }
       }
   }

    for(var row = 0;row < 20; row++){
        for(var col = 9; col >= 0; col--){
            if(cells[row][col].blockNum === fallingBlockNum){
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function moveDown(){
    for(var col = 0;col < 10;col++){
        if(cells[19][col].blockNum === fallingBlockNum){
            isFalling = false;
            return;
        }
    }
   for(var row = 18; row >= 0;row--){
       for(var col = 0; col < 10; col++){
           if(cells[row][col].blockNum === fallingBlockNum){
               if(cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
               isFalling = false ;
               return;   
               }
           }
       }
   }
    for(var row = 19;row >= 0; row--){
        for(var col = 9; col >= 0; col--){
            if(cells[row][col].blockNum === fallingBlockNum){
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

document.addEventListener("keydown", function onKeyDown(event){
    if(event.keyCode === 37){
        moveLeft();
    }else if(event.keyCode === 39){
        moveLight();
    }else if(event.keyCode === 40){
        moveDown();
    }
    })
//回転
//予測
//全部の行にブロックがあり、かつ一番上の段にブロックがあればゲームオーバー